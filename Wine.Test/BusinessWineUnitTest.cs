using Microsoft.VisualStudio.TestTools.UnitTesting;
using Wine.Commons.Business.Interfaces;
using Wine.Business.Services;
using System.Threading.Tasks;
using Wine.Commons.DAL.Interfaces;
using Wine.DataAccess;

namespace Wine.Test
{
    [TestClass]
    public class BusinessWineUnitTest // used to mock the concrete classes; integration is used to check in the DB
    {
        private IWineService _wineService;
        private IWineRepository _wineRepository;

        [TestInitialize]
        public void SetUp()
        {
            _wineRepository = new WineRerository();
            _wineService = new WineService(_wineRepository);
        }

        [TestMethod]
        public async Task GetWineById()
        {
            // *** test paterns ***

            // Arrange
            int id = 7;

            // Act

            var result = await _wineService.GetWineById(id);

            // Assert

            Assert.IsNotNull(result);
            Assert.IsTrue(id == 7);
            Assert.AreEqual(result.Name, "rioja");
        }

       
    }
}
