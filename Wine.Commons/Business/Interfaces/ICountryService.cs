﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Wine.Commons.Business.Models;

namespace Wine.Commons.Business.Interfaces
{
    public interface ICountryService
    {
        Task<CountryModel> GetAllCountriesByName(string countryName);

        Task<List<CountryModel>> GetAllCountries();

        Task<CountryModel> AddNewCountry(CountryModel newModel);

        Task<CountryModel> UpdateCountry(CountryModel updModel);

        Task<bool> DeleteCountryById(int id);
    }
}
