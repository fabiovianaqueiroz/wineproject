﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Wine.Commons.Business.Models;

namespace Wine.Commons.Business.Interfaces
{
    public interface IWineService 
    {
        Task<WineModel> GetWineById(int id);

        Task<List<WineModel>> GetWines();

        Task<WineModel> AddNewWine(WineModel newModel);

        Task<WineModel> UpdateWineById(WineModel updModel);

        Task<bool> DeleteWineById(int id);
    }
}
