﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Wine.Commons.Business.Interfaces;
using Wine.Commons.Business.Models;

namespace Wine.Business.Services
{
    public class RegionService : IRegionService
    {
        private IGenericRepository _repository;
        public RegionService(IGenericRepository repository)
        {
            _repository = repository;
        }

        public async Task<List<RegionModel>> GetAllRegions()
        {
            List<RegionModel> AllRegions = new List<RegionModel>();

            var getAllRegions = _repository.All<Wine.Data.Region>();

            foreach (Wine.Data.Region region in getAllRegions)
            {
                var response = new RegionModel()
                {
                    Name = region.Name,
                    CountryID = region.CountryID,
                    Wines = region.Wines?.Select(x => new WineModel
                    {
                        ID = x.ID,
                        Name = x.Name,
                        Description = x.Description,
                        Price = x.Price
                    }).ToList()
                };

                //var response2 = new RegionModel();

                //response2.Name = region.Name;
                //response2.CountryID = region.CountryID;

                //if (region.Wines != null)
                //{
                //    foreach (Wine.Data.Region item in getAllRegions)
                //    {
                //        foreach (Wine.Data.Wine wine in item.Wines)
                //        {
                //            response2.Wines.Add(new WineModel
                //            {
                //                ID = wine.ID,
                //                Name = wine.Name,
                //                Description = wine.Description,
                //                Price = wine.Price
                //            });
                //        }
                //    }                   
                //}
                //AllRegions.Add(response2);

                AllRegions.Add(response);
                
            }

            return AllRegions;
        }

        public async Task<RegionModel> GetRegionByName(string regionName)
        {
            var region = await _repository.GetSingleAsync<Wine.Data.Region>(x => x.Name == regionName);

            var displayRegion = new RegionModel()
            {
                Name = region.Name,
                CountryID = region.CountryID,
                Wines = region.Wines?.Select(x => new WineModel
                {
                    Name = x.Name,
                    Description = x.Description,
                    Price = x.Price

                }).ToList()
            };

            return displayRegion;
        }

        public async Task<RegionModel> AddNewRegion(RegionModel newModel)
        {
            var addCountry = _repository.Add<Wine.Data.Region>(new Wine.Data.Region
            {
                Name = newModel.Name,
                CountryID = newModel.CountryID               
            });

            newModel.Name = addCountry.Name;
            newModel.CountryID = addCountry.CountryID;

            return newModel;
        }

        public async Task<RegionModel> UpdateRegion(RegionModel updModel)
        {
            var updRegion = await _repository.GetSingleAsync<Wine.Data.Region>(x => x.Name == updModel.Name);

            updRegion.Name = updModel.Name;
            updRegion.CountryID = updModel.CountryID;

            _repository.Update<Wine.Data.Region>(updRegion);

            var response = new RegionModel()
            {
                Name = updRegion.Name,
                CountryID = updRegion.CountryID
            };

            return response;
        }

        public async Task<bool> DeleteRegionById(int id)
        {
            var delRegion = await _repository.GetSingleAsync<Wine.Data.Region>(x => x.ID == id);

            _repository.Delete<Wine.Data.Region>(delRegion);

            return true;
        }

        public async Task<bool> DeleteRegionByName(string name)
        {
            var delRegionName = await _repository.GetSingleAsync<Wine.Data.Region>(x => x.Name == name);

            _repository.Delete<Wine.Data.Region>(delRegionName);

            return true;
        }
    }
}
