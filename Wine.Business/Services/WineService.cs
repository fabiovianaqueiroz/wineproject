﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Wine.Commons.Business.Interfaces;
using Wine.Commons.Business.Models;

namespace Wine.Business.Services
{
    public class WineService : IWineService
    {
        private IGenericRepository _repository;
        public WineService(IGenericRepository repository)
        {
            _repository = repository;
        }

        // get all
        public async Task<List<WineModel>> GetWines()
        {
            List<WineModel> allWines = new List<WineModel>();

            var getAll = _repository.All<Wine.Data.Wine>();

            foreach (Wine.Data.Wine wine in getAll)
            {
                var response = new WineModel()
                {
                    ID = wine.ID,
                    Name = wine.Name,
                    Price = wine.Price,
                    Description = wine.Description
                };

                allWines.Add(response);
            }

            return allWines;          
        }

        // get one 
        public async Task<WineModel> GetWineById(int id)
        {
            var wine = await _repository.GetSingleAsync<Wine.Data.Wine>(x => x.ID == id);

            return new WineModel { Name = wine.Name, Price = wine.Price, Description = wine.Description };
        }

        // add a new wine
        public async Task<WineModel> AddNewWine(WineModel newModel)
        {
            var addWine = _repository.Add<Wine.Data.Wine>(new Wine.Data.Wine
            {
                Name = newModel.Name,
                Price = newModel.Price,
                Description = newModel.Description
            });

            newModel.ID = addWine.ID;
            newModel.Name = addWine.Name;
            newModel.Price = addWine.Price;
            newModel.Description = addWine.Description;

            return newModel;
        }

        // update 
        public async Task<WineModel> UpdateWineById(WineModel updModel)
        {
            var findModel = await _repository.GetSingleAsync<Wine.Data.Wine>(x => x.ID == updModel.ID);

            findModel.Name = updModel.Name;
            findModel.Price = updModel.Price;
            findModel.Description = updModel.Description;

            _repository.Update<Wine.Data.Wine>(findModel);

            var response = new WineModel()
            {
                ID = findModel.ID,
                Name = findModel.Name,
                Price = findModel.Price,
                Description = findModel.Description
            };

            return response;
        }

        // delete
        public async Task<bool> DeleteWineById(int id)
        {
            var delWine = await _repository.GetSingleAsync<Wine.Data.Wine>(x => x.ID == id);

            _repository.Delete<Wine.Data.Wine>(delWine);

            return true;
        }
        
    }
}
