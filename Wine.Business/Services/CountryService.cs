﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Wine.Commons.Business.Interfaces;
using Wine.Commons.Business.Models;

namespace Wine.Business.Services
{
    public class CountryService : ICountryService
    {
        private IGenericRepository _repository;
        public CountryService(IGenericRepository repository)
        {
            _repository = repository;
        }

        public async Task<List<CountryModel>> GetAllCountries()
        {
            List<CountryModel> AllCountries = new List<CountryModel>();

            var allCountries = _repository.All<Wine.Data.Country>();

            foreach (Wine.Data.Country country in allCountries)
            {
                var response = new CountryModel()
                {
                    Name = country.Name,
                    Regions = country.Regions.Select(x => new RegionModel { Name = x.Name }).ToList()
                };

                AllCountries.Add(response);
            }

            return AllCountries;
        }

        public async Task<CountryModel> GetAllCountriesByName(string countryName)
        {
            var getCountryName = await _repository.GetSingleAsync<Wine.Data.Country>(x => x.Name == countryName);

            var displayCountry = new CountryModel()
            {
                Name = getCountryName.Name,
                Regions = getCountryName.Regions.Select(x => new RegionModel { Name = x.Name }).ToList() 
            };

            return displayCountry;
        }

        public async Task<CountryModel> AddNewCountry(CountryModel newModel)
        {
            var addCountry = _repository.Add<Wine.Data.Country>(new Wine.Data.Country
            {
                Name = newModel.Name
            });

            newModel.Name = addCountry.Name;

            return newModel;
        }

        public async Task<CountryModel> UpdateCountry(CountryModel updModel)
        {
            var updCountry = await _repository.GetSingleAsync<Wine.Data.Country>(x => x.Name == updModel.Name);

            updCountry.Name = updModel.Name;

            _repository.Update<Wine.Data.Country>(updCountry);

            var response = new CountryModel()
            {
                Name = updCountry.Name
            };

            return response;
        }

        public async Task<bool> DeleteCountryById(int id)
        {
            var delCountry = await _repository.GetSingleAsync<Wine.Data.Country>(x => x.ID == id);

            _repository.Delete<Wine.Data.Country>(delCountry);

            return true;
        }
    }
}
