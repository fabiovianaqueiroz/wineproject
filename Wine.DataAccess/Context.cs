﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using System.Configuration;
using Microsoft.Extensions.Configuration;

namespace Wine.DataAccess
{
    public class Context : DbContext
    {
        public Context() : base()
        {
         
        }
        public Context(DbContextOptions<Context> options) : base(options)
        {

        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            IConfigurationRoot configuration = new ConfigurationBuilder()
            .Build();

            optionsBuilder.UseSqlServer(configuration.GetConnectionString("DefaultConnection"));
        }

        // tables for the database

        public DbSet<Wine.Data.Wine> Wines { get; set; }

        public DbSet<Wine.Data.Country> Countries { get; set; }

        public DbSet<Wine.Data.Region> Regions { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Wine.Data.Wine>().ToTable("Wines");

            modelBuilder.Entity<Wine.Data.Country>().ToTable("Countries");

            modelBuilder.Entity<Wine.Data.Region>().ToTable("Regions");
        }
    }
}
