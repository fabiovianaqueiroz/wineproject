﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Wine.WebAPI.Migrations
{
    public partial class updatewine : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "Country",
                table: "Wines",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Description",
                table: "Wines",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Country",
                table: "Wines");

            migrationBuilder.DropColumn(
                name: "Description",
                table: "Wines");
        }
    }
}
